# abstract_orders
This is a test task from Russian company for Python/Django position.

<p>Statement of a problem:</p>
One need to implement simplified system of abstract orders.
There are customers, performers and system.
For simplicity customers will be acted by system's administrators. 
Administrators publish the order indicating its value.
Creating orders may be done by standard Django admin.
<br>
Performer (general customer) see tape (public for all authenticated users)
of the orders accessible for execution.
Performer click to "do" on the order, then amount enters in an account
with the deduction of system's commission.
One order may has only one performer. When the order is done, performer 
will be disappeared.
<br>
Technologies:
<br>Django
<br>PostgreSQL
<br>Client side: Twitter Bootstrap
<br>If there is some experience, one may use frameworks for client such as Backbone/Angular/React/etc.
<br>But it is permissible to use standard Django templates.
<br>It is important to focus attention on transactions, providing insurance against race-conditions
when processing orders.
